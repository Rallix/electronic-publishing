# EPD | PB029

Final assignments for the Electronic Publishing course (autumn 2020).  
LaTeX and XML parts are separated in their own branches (`tex` a `xml`).
